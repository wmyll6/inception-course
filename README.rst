Python Inception course
=======================

Program
-------

* Intro (day1)
    * Fascinating world, life style
    * Conventions (Readability counts)
    * Documentation
    * Testing
    * Environment ( GNU/Linux/OS X )
    * Ecosystem

* Project structure (day2, practical)
     * Tools to support project life-cycle
        - git, shell, pip, virtualenv (wrapper),

* Unittest (day 5)
    - concepts
    - examples

* Dive into language (day3)
    * Basics (questions)
    * OOP
    * FP (mutability)/top modules

* Intro into testing frameworks (day4)
    - general terms and concepts
    - pytest
    - nose
    - unittest
    - **TODO**: add more per testing approach

* Nose (day 6)
    - concepts
    - examples

* Pytest (day 7)
    - concepts
    - examples

* Application distribution (day 8)
    - setup.py
    - setuptools
    - sdist
    - egg/wheel

* Integration with CI (day 9)
    - travis
    - jenkins

* Expert tools to manage project lifecycle (day 10)
    - setuptools [none]
    - fabfile [none]
    - Makefile [none]
